import ccxt
import pprint
import pandas as pd
import numpy as np
import pyupbit
import time
import datetime
api_key = ''
secret = ''

access = ""
secrett = ""
upbit = pyupbit.Upbit(access, secrett)
print("autotrade start")

# binance 객체 생성
binance = ccxt.binance(config={
    'apiKey': api_key,
    'secret': secret,
    'enableRateLimit': True,
})
#매수리스트
buy_list = ["XRP/USDT"]
buy_cost = 0.9364
# 계좌 잔고 조회
deposit = binance.fetch_balance(params={"type": "future"})['USDT']


#일봉 조회,목표가 설정

def get_target_price(ticker,k):
    XRP_ohlcvs = binance.fetch_ohlcv(ticker,'1d')
    df = pd.DataFrame(XRP_ohlcvs, columns=['datetime', 'open', 'high', 'low', 'close', 'volume'])
    df['datetime'] = pd.to_datetime(df['datetime'], unit='ms')
    df.set_index('datetime', inplace=True)
    df['range'] = (df['high'] - df['low']) * k
    df['target'] = df['open'] - df['range'].shift(1)
    df['ror'] = np.where(df['high'] > df['target'],
                         df['close'] / df['target'],
                         1)
    df['hpr'] = df['ror'].cumprod()
    df['dd'] = (df['hpr'].cummax() - df['hpr']) / df['hpr'].cummax() * 100
    today_open = df['close'][-2]
    yesterday_high = df['high'][-2]
    yesterday_low = df['low'][-2]
    target = today_open -(yesterday_high- yesterday_low)*0.5
    return target


def get_current_price(i):
    price = binance.fetch_ticker(i)['close']
    return price



def get_start_time(ticker):
    """시작 시간 조회"""
    df = pyupbit.get_ohlcv(ticker, interval="day", count=1)
    start_time = df.index[0]
    return start_time
# 자동매매 시작

while True:
    try:
        now = datetime.datetime.now()
        start_time = get_start_time("KRW-BTC") - datetime.timedelta(seconds=50)
        end_time = start_time + datetime.timedelta(days=1)

        #i값 설정
        i = "XRP/USDT"
        if start_time < now < end_time - datetime.timedelta(seconds=30):
        # if start_time > now :
            target_price = get_target_price(i, 0.5)
            current_price = get_current_price(i)
            if target_price < current_price and i not in buy_list:
                print("매수기준 충족 매수실시!!!")
                usdt = deposit['free']
                if usdt > 1:
                    order = binance.create_market_sell_order(
                        symbol='XRP/USDT',
                        amount=round((usdt/3)/current_price,0),
                        params={'type': 'future'}
                    )
                    pprint.pprint(order)
                    buy_cost = current_price
                    buy_list.append(i)
        else :
            coin = deposit['used']
            if coin > 0.00008 and i in buy_list:
                order = binance.create_market_buy_order(
                    symbol='XRP/USDT',
                    amount=round(coin*0.9995/buy_cost,1),
                    params={'type': 'future'}
                )
                pprint.pprint(order)
                buy_list.remove(i)
        time.sleep(1)
    except Exception as e:
        print(e)
        time.sleep(1)


