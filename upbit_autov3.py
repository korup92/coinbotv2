import time
import pyupbit
import datetime

access = ""
secret = ""
a = ["BTC", "ADA","EDR","SAND","CHZ","ATOM","HUNT","HUM","BTC","DOGE","SNT","XRP","ETH","EOS","STX","BCH","QTUM"]
buy_list = []



# 변동성 돌파 전략으로 매수 목표가 조회
def get_target_price(ticker, k):
    df = pyupbit.get_ohlcv(ticker, interval="day", count=2)
    target_price = df.iloc[0]['close'] + (df.iloc[0]['high'] - df.iloc[0]['low']) * k
    return target_price


#시작 시간 조회
def get_start_time(ticker):
    df = pyupbit.get_ohlcv(ticker, interval="day", count=1)
    start_time = df.index[0]
    return start_time
# 잔고 조회
def get_balance(ticker):
    balances = upbit.get_balances()
    for b in balances:
        if b['currency'] == ticker:
            if b['balance'] is not None:
                return float(b['balance'])
            else:
                return 0
#    현재가 조회
def get_current_price(ticker):
    return pyupbit.get_orderbook(tickers=ticker)[0]["orderbook_units"][0]["ask_price"]

# 로그인
upbit = pyupbit.Upbit(access, secret)
print("autotrade start")

# 자동매매 시작

while True:
    for i in a:
        try:
            now = datetime.datetime.now()
            start_time = get_start_time("KRW-"+str(i)) - datetime.timedelta(seconds=60)
            end_time = start_time + datetime.timedelta(days=1) - datetime.timedelta(hours=4)
            if start_time < now < end_time :
                target_price = get_target_price("KRW-"+str(i), 0.5)
                current_price = get_current_price("KRW-"+str(i))
                print("KRW-"+str(i),"현재가!!:",current_price,"매수기준가!!",target_price)
                if 0 < current_price and "KRW-"+str(i) not in buy_list:
                    print("매수기준 충족!! 매수실시!!!")
                    krw = get_balance("KRW")
                    print(buy_list.append("KRW-"+str(i)))

                    if krw > 5000:
                        # print(upbit.buy_market_order("KRW-"+str(i), krw*0.3))
                        print("매수주문을 요청하였습니다.")
            else:
                btc = get_balance(i)
                if btc > 0.00008:
                    upbit.sell_market_order("KRW-"+str(i), btc*0.9995)
                    # print(upbit.sell_market_order("KRW-"+str(i), btc*0.9995))
                    print("매도주문을 요청하였습니다.")
                    buy_list.remove("KRW-"+str(i))
            time.sleep(1)
        except Exception as e:
            print(e)
            time.sleep(1)
#5.25